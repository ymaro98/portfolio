var allProjects = [
    {
        name: 'HCI',
        prefix: 'hci',
        bgImage: 'zero_state.png',
        description: 'Een Pinqpong Custom Bags actiesite',
        full_description: "\
        <span>Opdracht:</span><br>\
        De opdracht voor HCI was om een Custom Bags actiesite te ontwerpen om dus je eigen tas te ontwerpen.\
        Er zijn verschillende opties mogelijk, zoals: het formaat van de tas, kleuren aanpassen, logos voor op de tas of schouderband.\
        Ook kan er gekozen worden voor accessoires, zoals: laptopvoedingen, handvaten, laptophoesjes en meer.\
        <br><br>\
        Waarom ik dit project zo leuk vind en vooral ook waarom ik zo trots ben, is omdat ik bij het maken van het ontwerp echt het gevoel\
        had dat het ontwerp dat ik had gemaakt functioneel was en ook goed in elkaar zat.\
        ",
        project_images: [
            'binnenvoering.png',
            'branding_schouderblad_error.png',
            'fwf.png',
            'kies_een_formaat.png',
            'kies_kleuren_accent_afsluitflap.png',
            'zero_state.png',
        ],
        link: 'https://xd.adobe.com/view/aff0f8ab-fa8f-4458-a1ca-bca59304f69c-faa3/',
        date: '',
        msg: {
            top: '',
            bottom: 'De link naar xd is geen prototype maar een link met alle schermen',
        },
    },
    {
        name: 'Internetstandaarden',
        prefix: 'interstdn',
        bgImage: 'home_bg.jpeg',
        description: 'Playstation 5 News website',
        full_description: "\
        <span>Opdracht:</span><br>\
        Bij deze opdracht was het de bedoeling om een website te maken voor een onderwerp naar keuze.\
        De playstation 5 was nog niet uit, maar was wel een hot topic en was er zelf ook geinterreseerd in, dus besloot ik het daarover te hebben.\
        <br><br>\
        Voordat ik dit vak kreeg was ik al best wel goed in html/css, maar ik ben zeker blij dat ik het vak heb gekregen \
        , omdat ik nu weet hoe flex-box werkt, terwijl ik eerst bijna een hekel had aan flex-box en ook dacht dat het overbodig was. \
        Daarnaast maak ik ook beter gebruik van goede en semantische tags, zodat de browser goed weet waar alle sections voor gebruikt worden. Denk aan een artikel, navigatie of footer\
        En ik ben vooral trots op het eindresultaat dat ik heb geleverd, want al zeg ik het zelf... Ik vind het er zeker goed uit zien!\
        ",
        project_images: [
            'home.jpeg',
            'games.jpeg',
            'dualsense.jpeg',
            'preorder.jpeg',
        ],
        link: 'https://oege.ie.hva.nl/~blueyww/internetstandaarden/',
        date: '',
        msg: {
            top: '',
            bottom: '',
        }
    },
    {
        name: 'SRP<br>&<br>Inleiding Programmeren',
        prefix: 'srp',
        bgImage: 'mariobg.png',
        description: 'Interactive Mario Game',
        full_description: "\
        <span>Opdracht:</span><br>\
        Voor het vak inleiding programmeren is het de bedoeling dat je een interactief spel of webapplicatie maakt.\
        Van de docent mocht ik verder werken aan <a href=\"https://oege.ie.hva.nl/~blueyww/spelenmetcss/\">een project wat ik in een srp week had gemaakt</a> \
        Het project wat ik toen had gemaakt, was Mario die verschillende animaties deed met verschillende button clicks. Die was al bijna goed genoeg\
        , maar hij had toch meer interactiviteit nodig op het vlak van verschillende states.\
        Dus toen heb ik er een heel spel van gemaakt waarbij een menu hebt met verschillende soorten settings, zoals: geluid en moeilijkheidsgraad \
        En als je nu dus een animatie met mario wilt doen, moet je eerst genoeg energie hebben die je dus krijgt door sterren te pakken \
        , want als hij niet genoeg energie heeft, valt hij weer in slaap.\
        <br><br>\
        Bij dit vak ben ik vooral trots dat ik een eerder gemaakt project van mij naar een nog hoger level heb kunnen tillen met veel complexere code.\
        ",
        project_images: [
            'instructies.jpeg',
            'mario.jpeg',
            'mario2.0.jpeg',
            'menu.jpeg',
        ],
        link: 'https://oege.ie.hva.nl/~blueyww/mario/',
        date: '',
        msg: {
            top: '',
            bottom: '',
        }
    },
    {
        name: 'Project Individueel 1: ISGA',
        prefix: 'pji1',
        bgImage: 'home_bg.jpeg',
        description: 'Companion App voor het ISGA-boekje',
        full_description: "\
        <span>Opdracht:</span><br>\
        De bedoeling van de opdracht is: een mobiele web-app maken die hand-in-hand gaat met het ISGA-boekje. \
        De app moet ontworpen worden en daarna uitgewerkt met HTML en CSS. Daarnaast moet de web-app ook de \
        huisstijl van het ISGA-boekje aanhouden.\
        <br><br>\
        <span>Mijn concept in het kort</span>: <br>Met de Noted compagnon kan je een account aanmaken om daarmee bezienswaardigheden te bezoeken samen met andere buitenlandse studenten.\
        <br><br>\
        Project Individueel begon niet heel erg super. Ik had heel veel moeite om op een concept te komen, waardoor ik dacht dat ik een onvoldoende zou halen.\
        Maar toen ik nou eenmaal mijn concept had, ging alles super goed!\
        Bij Project Individueel 1 was ik erg tots dat ik mijn concept heb kunnen vertalen naar de html/css paginas. Deze was \"zo goed\" \
        dat mijn docent mijn project als voorbeeld had gebruikt.\
        En het is natuurlijk mijn eerste project, dus of het nou goed of fout ging had ik zeker wel mijn best gedaan.\
        ",
        project_images: [
            'activity_page.png',
            'invitation_screen.png',
            'invite_creation.png',
            'joined_students.png',
            'storyboard.png',
        ],
        link: 'https://oege.ie.hva.nl/~blueyww/invited/',
        date: '',
        msg: {
            top: '',
            bottom: 'Project is ontworpen voor mobile view met max width van 425px',
        }
    },
    {
        name: 'Project Individueel 2: Artis Experience',
        prefix: 'pji2',
        bgImage: 'home_bg.png',
        description: 'New Artis Experience Concept',
        full_description: "\
        <span>Opdracht:</span><br>\
        De bedoeling is dat het kind en niet de ouder \
        de weg bepaalt. Hier moet een creatief en interactief concept voor bedacht worden gericht\
        naar de jonge dierenvriend tussen de 7 en 12 jaar.\
        Dit concept moet meerdere problemen oplossen, maar minimaal de manier hoe je door\
        het park navigeert en show/voertijden kunt checken. En daarnaast ook het drukte probleem\
        tijdens de piektijden oplossen.\
        Dit concept moet leuk en geschikt zijn voor kinderen tussen de 7 en 12 jaar uit verschillende\
        landen.\
        <br><br>\
        <span>Mijn concept in het kort</span>: <br> \
        Elk kind dat het park binnenkomt is een Partis Ranger. En de ouder/verzorger is een Assistent Partis Ranger.\
        De Assistent Partis Rangers helpen met het bijhouden van de companion app “Partis Boek” en het is dan ook de bedoeling dat zij dit bij zich houden, zodat het kind iets meer vrijheid heeft en niet de hele tijd aan de telefoon gekleefd zitten.\
        Het Partis Boek helpt met het navigeren door het park, meedoen met de quiz, badges verzamelen en het mysterieuze \"Easter Egg Mystery\"\
        <br><br>\
        Bij Project Individueel 2 was ik erg trots dat ik met een geheel eigen concept kon komen en hier dan ook een conceptvideo voor gemaakt heb.\
        Ik vond vooruit kijken bij dit project erg lastig, want toen ik hoorde wat er allemaal gedaan moest worden, had ik nooit gedacht dat dit zou lukken.\
        ",
        project_images: [
            'badge_screen.png',
            'badges.png',
            'home_menu_w_quiz.png',
            'omleiding.png',
            'quiz.png',
        ],
        link: 'https://xd.adobe.com/view/a608a065-762c-460c-90c0-c9156081e5f8-f35b/?fullscreen=true',
        date: '',
        msg: {
            top: '',
            bottom: 'Dit project is ontworpen voor tablet en mobile size',
        }
    },
]