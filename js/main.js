var body = document.querySelector('body');
var toggle = document.querySelector('.toggle');
var nav = document.querySelector('nav');
var projectsContainer = document.querySelector('.projects-container');
var scrollLinks = document.querySelectorAll('nav > ul > li > a, .skills-btn > a, .projects-btn > a');
var projects;
var projectsHtml = '';
var mobileWidth = 750;

const toggleNav = () => {
    toggle.classList.toggle('active');
    nav.classList.toggle('active');
    
    if(window.innerWidth < mobileWidth){
        toggleBodyScroll();
    }
}

const toggleBodyScroll = () => {
    body.classList.toggle('no-scroll');
}

const setProjectsBg = (e) => {

    projects.forEach(project => {
        let bg = project.dataset.bg;
        let prefix = `images/projects`
        project.style.backgroundImage = `url(${prefix}/${bg})`;
    });
}

const toggleProjectDetails = (e) => {
    e.target.classList.toggle('show');
}

const setProjectsHover = () => {

    projects.forEach(project => {
        project.addEventListener('click', toggleProjectDetails);
        project.addEventListener('mouseenter', toggleProjectDetails);
        project.addEventListener('mouseleave', toggleProjectDetails);
    });
}

const loadProjects = () => {

    allProjects.forEach((project, index) => {

        let project_data = JSON.stringify(project);
        let bgImage = `${project.prefix}/${project.bgImage}`;
        projectsHtml += 
        `
        <div data-bg="${bgImage}" class="project">
            <div>
                <span>${index + 1}</span>
                <p class="heading">${project.description}</p>
                <p class="details">${project.name}</p>
                <button data-project='${project_data}' class="show-modal">Lees meer</button>
            </div>
            <span>${project.name.replaceAll("<br>", " ")}<i class="fas fa-info-circle"></i></span>
            
        </div>    
        `;
    });

    projectsContainer.innerHTML = projectsHtml;
}

const checkElement = async selector => {
    while ( document.querySelector(selector) === null) {
      await new Promise( resolve =>  requestAnimationFrame(resolve) )
    }
    return document.querySelector(selector); 
};

//smooth scroll
scrollLinks.forEach(function(link){

    link.addEventListener("click", function(e){

        e.preventDefault();

        let linkType = e.target.parentNode.nodeName;
        if(linkType == 'LI'){
            toggleNav();
        }
        
        const id = e.currentTarget.getAttribute('href').slice(1);
        const element = document.getElementById(id);
        const navHeight = nav.getBoundingClientRect().height;
        let position = (element != null) ? element.offsetTop - navHeight : 0;

        window.scrollTo({
            top:position,
        });
    })
})

toggle.addEventListener('click', toggleNav);
window.addEventListener('DOMContentLoaded', loadProjects);

document.addEventListener("DOMNodeInserted", (e) => {
    projects = document.querySelectorAll('.project');
    setProjectsBg();
    setProjectsHover();
});
  