var modalBtns;
var modal = document.querySelector('.modal');
var modalContent = document.querySelector('.modal .modal-content div');
var modalImageContainer;
var closeButton  = document.querySelector(".close-button");

const setModalImagesScroll = () => {
    modalImageContainer.addEventListener('mousedown', mouseDownHandler);
}

const mouseDownHandler = function(e) {
    modalImageContainer.style.cursor = 'grabbing';
    modalImageContainer.style.userSelect = 'none';

    pos = {
        left: modalImageContainer.scrollLeft,
        x: e.clientX,
    };

    document.addEventListener('mousemove', mouseMoveHandler);
    document.addEventListener('mouseup', mouseUpHandler);
}

const mouseMoveHandler = function(e) {
    const dx = e.clientX - pos.x;
    modalImageContainer.scrollLeft = pos.left - dx;
};

const mouseUpHandler = function() {
    modalImageContainer.style.cursor = 'grab';
    modalImageContainer.style.removeProperty('user-select');

    document.removeEventListener('mousemove', mouseMoveHandler);
    document.removeEventListener('mouseup', mouseUpHandler);
};

const closeModal = () => {

    toggleBodyScroll();
    modal.classList.remove('show-modal');
    modalContent.innerHTML = '';
}

const checkWindowClick = (e) => {
    if (e.target === modal) {
        closeModal();
    }
}

const openModal = (e) => {

    toggleBodyScroll();
    let project = JSON.parse(e.target.dataset.project); 
    modal.classList.add('show-modal');

    let modalHtml = '';
    modalHtml += `
        <h1>${project.name}</h1>
        <span>Afbeeldingen uit project:</span>  
        <div class="project-images">
    `;

    project.project_images.forEach(img => {
        let imgPrefix = `images/projects/${project.prefix}`
        modalHtml += `
            <div>
                <img draggable="false" src="${imgPrefix}/${img}" alt="">
            </div>    
        `;
    });

    let link = (project.link != '') 
    ? `<a href="${project.link}" target="_blank">Link naar project <i class="fas fa-link"></i></a>` 
    : '';

    let msg_btm = (project.msg.bottom != '') 
    ? `<span class="btm">${project.msg.bottom}</span>` 
    : '';

    modalHtml += `
        </div>
        <p class="full-description">${project.full_description}</p>
        ${msg_btm}
        ${link}
    `;

    modalContent.innerHTML = modalHtml;

    checkElement('.project-images').then((selector) => {
        modalImageContainer = document.querySelector(".project-images");
        setModalImagesScroll();
    })
}

const setupModals = () => {
 
    modalBtns = document.querySelectorAll('.show-modal');

    modalBtns.forEach(btn => {
        btn.addEventListener('click', openModal);
    });
}

closeButton.addEventListener("click", closeModal);
window.addEventListener("click", checkWindowClick);

window.onload = function() {
    checkElement('.show-modal').then((selector) => {
        setupModals();
    })
};